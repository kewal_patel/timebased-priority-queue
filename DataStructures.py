from datetime import datetime
import time
import operator

class CustomNode(object):
  
	def __init__(self,name,date_time,priority):
		self.name = self.clean_name(name)
		self.date_time = self.clean_time(date_time)
		self.priority = self.clean_priority(priority)

	def clean_name(self,value):
		if value.startswith('"') or value.startswith("'"):
			return value[1:-1]

	def clean_priority(self,value):
		if type(value) == str:
			value = value.strip().strip('\n').strip(';')
		return int(value)

	def clean_time(self,value):
		value = value.strip().strip('\n').strip(';')
		if value.startswith('"'):
			value =  value[1:-1]
		return datetime.strptime(value,"%Y/%m/%d %H:%M")

	def get_time(self):
		return datetime.strftime(self.date_time,"%Y/%m/%d %H:%M")

	def __str__(self):
		return "Current Time [ %s ], Event %s processed, Priority %s" %(self.get_time(), self.name, self.priority)

	def __lt__(self,other):
		if self.date_time == other.date_time:
			if self.priority == 0:
				return False
			elif other.priority == 0:
				return True
			else:
				return self.priority < other.priority
			# return self.priority < other.priority
		return self.date_time < other.date_time


class CustomPriorityQueue(object):
	""" Custom Priority Queue for time based node removal and insertion """

	def __init__(self):
		self.items = []
		self.counter = 0

	def __iter__(self):
		self.count = 0
		return self

	def __next__(self):
	    if self.count == self.counter:
	        raise StopIteration
	    self.count += 1
	    return self.items[self.count - 1]

	def total_tasks(self):
		return self.counter

	def clean_time(self,value):
		value = value.strip().strip('\n').strip(';')
		if value.startswith('"') :
			value =  value[1:-1]
		return datetime.strptime(value,"%Y/%m/%d %H:%M")		

	def execute_tasks(self, start_time):
		start_time = self.clean_time(start_time)
		task_list = sorted([ obj for obj in self.items if obj.date_time >= start_time])
		print("Tasks to be executed :",len(task_list))
		for i in range(0,len(task_list)):
			print(task_list[i])
			if i < len(task_list)-1:
				time_till_next_task = task_list[i+1].date_time - task_list[i].date_time
				# calculating time to sleep
				time.sleep(time_till_next_task.total_seconds())
			self.items.remove(task_list[i])
			self.counter -= 1
		return None

	def insert(self, item):
		self.items.append(item)
		self.counter += 1
