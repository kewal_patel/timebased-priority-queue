import sys
from DataStructures import CustomPriorityQueue, CustomNode

def get_input():
	if len(sys.argv) == 3:
		file_name = sys.argv[1]
		start_time = sys.argv[2]
		print("Filename is :", file_name)
		print("Schedule Time is :", start_time)
		return (file_name,start_time)
	else:
		raise ValueError("Please Provide Input file and start time.")	


def start_execution(file_name,start_time):
	queue = CustomPriorityQueue()

	with open(file_name,"r") as file:
	    file_contents = file.readlines()[1:]
	    for line in file_contents:
	    	line_content = line.strip('\n').strip(';').split(",")
	    	# checking if priority is present or not
	    	if len(line_content) > 2:
	    		task_name, task_time, task_priority = line_content
	    	else:
	    		task_name, task_time = line_content
	    		task_priority = 0
	    	queue.insert(CustomNode(task_name, task_time,task_priority))

	print("Total Tasks in Queue :", queue.total_tasks())
	queue.execute_tasks(start_time)

if __name__ == "__main__":
	file_name, start_time = get_input()
	start_execution(file_name, start_time)
else:
	print("Please Enter name of the file and schedule time as arguments.")